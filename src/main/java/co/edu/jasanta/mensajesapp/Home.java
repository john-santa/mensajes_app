package co.edu.jasanta.mensajesapp;

import java.sql.SQLException;
import java.util.Scanner;

import co.edu.jasanta.mensajesapp.Service.MensajesService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Home {

    private static final Logger logger = LoggerFactory.getLogger(Home.class);

    public static void main(String[] args) throws SQLException {
        Scanner scanner = new Scanner(System.in);
        int opcion;

        do {
            System.out.println("-----------------");
            System.out.println("Aplicacion de mensajes");
            System.out.println("1. Crear mensaje");
            System.out.println("2. Listar mensajes");
            System.out.println("3. Eliminar mensaje");
            System.out.println("4. Editar mensaje");
            System.out.println("5. Salir");

            opcion = scanner.nextInt();

            switch (opcion) {
                case 1:
                    MensajesService.crearMensaje();
                    break;
                case 2:
                    MensajesService.listarMensajes();
                    break;
                case 3:
                    MensajesService.borrarMensajes();
                    break;
                case 4:
                    MensajesService.editarMensaje();
                    break;
                default:
                    break;
            }
        } while (opcion != 5);


    }
}
