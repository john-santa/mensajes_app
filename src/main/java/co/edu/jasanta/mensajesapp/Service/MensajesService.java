package co.edu.jasanta.mensajesapp.Service;

import co.edu.jasanta.mensajesapp.DAO.MensajesDao;
import co.edu.jasanta.mensajesapp.Model.Mensajes;

import java.sql.SQLException;
import java.util.Scanner;

public class MensajesService {

    public static void crearMensaje() throws SQLException {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Escribe tu mensaje");
        String mensaje = scanner.nextLine();

        System.out.println("Tu nombre");
        String autor_mensaje = scanner.nextLine();

        Mensajes registro = new Mensajes();
        registro.setMensaje(mensaje);
        registro.setAutor_mensaje(autor_mensaje);

        MensajesDao.crearMensajeDB(registro);
    }

    public static void listarMensajes(){
        MensajesDao.leerMensajeDB();
    }

    public static void borrarMensajes(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Indica el Id del mensaje a borrar");
        Integer id_mensaje = scanner.nextInt();
        MensajesDao.borrarMensajeDB(id_mensaje);
    }

    public static void editarMensaje(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Indica el Id del mensaje a actualiazar");
        Integer id_mensaje = scanner.nextInt();
        System.out.println("Indica el el mensaje a actualiazar");
        String  mensaje = scanner.nextLine();
        Mensajes mensajeActualizar = new Mensajes();
        mensajeActualizar.setMensaje(mensaje);
        mensajeActualizar.setId_mensaje(id_mensaje);
        MensajesDao.actualizarMensajeDB(mensajeActualizar);
    }
}
