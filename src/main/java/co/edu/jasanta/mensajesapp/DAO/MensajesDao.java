package co.edu.jasanta.mensajesapp.DAO;

import co.edu.jasanta.mensajesapp.DB.Conexion;
import co.edu.jasanta.mensajesapp.Model.Mensajes;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MensajesDao {

    public static void crearMensajeDB(Mensajes mensaje) {
        Conexion conexion = new Conexion();

        try {
            Connection connection = conexion.getConnection();
            PreparedStatement preparedStatement = null;

            String query = "INSERT INTO `mensajes` (`mensaje`, `autor_mensaje`) VALUES (?, ?)";
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, mensaje.getMensaje());
            preparedStatement.setString(2, mensaje.getAutor_mensaje());
            preparedStatement.executeUpdate();
            System.out.println("Mensaje creado correctamente");

        }catch (SQLException exception){
            System.out.println(exception);
        }
    }

    public static void leerMensajeDB(){
        Conexion conexion = new Conexion();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            Connection connection = conexion.getConnection();
            String query = "SELECT * FROM mensajes";
            preparedStatement = connection.prepareStatement(query);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()){
                System.out.println("ID: "+resultSet.getInt("id_mensaje"));
                System.out.println("MENSAJE: "+resultSet.getString("mensaje"));
                System.out.println("AUTOR: "+resultSet.getString("autor_mensaje"));
                System.out.println("AUTOR: "+resultSet.getString("fecha_mensaje"));

            }

        } catch (SQLException exception) {
            System.out.println("No se pudieron recuperar los mensajes");
            exception.printStackTrace();
        }
    }

    public static void borrarMensajeDB(Integer id_mensaje){
        Conexion conexion = new Conexion();

        try {
            Connection connection = conexion.getConnection();
            PreparedStatement preparedStatement = null;

            String query = "DELETE FROM mensajes WHERE id_mensaje = ?";
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, id_mensaje);
            preparedStatement.executeUpdate();

            System.out.println("Mensaje eliminado");

        } catch (SQLException exception) {
            System.out.println("No se pudo borrar el mensaje");
            exception.printStackTrace();
        }
    }

    public static void actualizarMensajeDB(Mensajes mensaje){
        Conexion conexion = new Conexion();
        try {
            Connection connection = conexion.getConnection();
            PreparedStatement preparedStatement = null;

            String query = "UPDATE mensajes SET mensaje = ? WHERE id_mensaje = ?";
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, mensaje.getMensaje());
            preparedStatement.setInt(2, mensaje.getId_mensaje());
            preparedStatement.executeUpdate();
            System.out.println("Mensaje actualiado con exito!!!");

        }catch (SQLException exception){
            System.out.println(exception);
            System.out.println(exception.getMessage());
        }
    }
}
